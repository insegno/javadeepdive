
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class HelloWorld {

    public static void main(String[] args) {


        /**
         * Tipi di dato primitivi
         */
        byte numeroByte = -128;
        short numeroShort;
        int numeroInt;
        long numeroLong;
        float numeroFloat;
        double numeroDouble;
        boolean booleaneo;


        /**
         * Array normali
         */

        // Array interi
        int[] arrayIntero = {1, 5, 0, 230};

        // modificare un elemento dell'array
        // accedo all'indice dell'elemento
        arrayIntero[3] = 23;

        // lunghezza array
        System.out.println("Lunghezza arrayintero: " + arrayIntero.length);

        // stampare Array
        System.out.println(Arrays.toString(arrayIntero) + "\n");


        /**
         * ArrayList
         */

        // TipoVariabile<TipoOggettoNellaLista> = new TipoVariabile<>()
        ArrayList<String> lista = new ArrayList<>();

        // aggiungere elementi
        lista.add("ciao");
        lista.add("hello");
        lista.add("hola");

        System.out.println(lista.toString());

        // rimuovere un elemento
        // posso farlo sia con l'indice dell'elemento partendo da 0
        // sia con Object stesso
        lista.remove("hola");
        lista.remove(1);

        System.out.println(lista.toString() + "\n");

        /**
         * Operatori condizionali
         */

        int a = 24;
        int b = 34;
        int c = 24;

//        if (a > b) {
//            System.out.println(a + " è maggiore di " + b);
//
//        } else if (a == b) {
//            System.out.println(a + " è uguale a " + b);
//
//        } else {
//            System.out.println(a + " non è ne maggiore ne uguale a " + b);
//        }
//

        ////////////////////////////////////////

        if (a < b) {
            System.out.print("a < b");
        }

        ////////////////////////////////////////

        if (a > b) {

            System.out.print("a > b");

        } else {

            System.out.print("a > b false");
        }

        ////////////////////////////////////////

        if (a > b) {

        }

        else if (a >= b) {

        }

        else if (a <= b) {

        }

        ////////////////////////////////////////

        String saluto = "ciaoo";

        switch (saluto) {
            case "ciao":
                System.out.print("\nciao\n");
                break;
            case "hello":
                System.out.print("\nhello\n");
                break;
            case "hola":
                System.out.print("\nhola\n");
                break;
            default:
                System.out.print("\nNon ti saluto\n");
        }

        ////////////////////////////////////////

        if (a < b) {

            if (a < c) {
                System.out.println(a + " è minore o uguale a " + c);

            } else {

            }
        }


        if ((true || false) && (2 == 2)) {
            System.out.println("vero" + "\n");
        }

        /**
         * Operatore unario
         */

        boolean flag = true;
        System.out.println(flag);
        flag = !flag;
        System.out.print(flag + "\n");


        int num = 21;
        boolean pari = (num % 2 == 0) ? true : false;
        if (!pari) {
            System.out.print(num + " è dispari\n");

        } else {
            System.out.print(num + " è pari\n");
        }

        /**
         *
         */
        ArrayList<Integer> arrayinteri = new ArrayList<>();
        arrayinteri.add(3);
        arrayinteri.add(9);
        arrayinteri.add(67);

        if (arrayinteri.get(0) > arrayinteri.get(1) && arrayinteri.get(0) > arrayinteri.get(2)) {
            System.out.println("Il primo elemento è il maggiore");

        } else if (arrayinteri.get(1) > arrayinteri.get(2) && arrayinteri.get(1) > arrayinteri.get(0)) {
            System.out.println("Il secondo elemento è il maggiore");

        } else {
            System.out.println("Il terzo elemento è il maggiore");
        }

        String result = (20 % 2 == 0) ? "il numero è pari" : "il numero è dispari";


        ////////////////////////////////////////
        // do while
        System.out.println();

        int index = 0;

        do {

            System.out.println(index);
            index++;

        } while (index < 10);

        ////////////////////////////////////////
        // while
        System.out.println();

        index = 0;

        while (index < 10) {
            System.out.println(index);
            index++;
        }

        ////////////////////////////////////////
        // for
        System.out.println();

        int i;

        for (i = 0; i < 10; i++) {
            System.out.println(i);
        }

        ////////////////////////////////////////
        // stampare primi 10 numeri pari con while
        System.out.println();

        int numero = 1;

        while (numero <= 10) {
            if (numero % 2 != 0) {
                System.out.println(numero);
            }

            numero++;
        }

        ////////////////////////////////////////
        // stampare primi 10 numeri pari con while
        System.out.println();

        long num1 = 0;
        long num2 = 1;
        long num3;

        System.out.println(num1);
        System.out.println(num2);

        for(i = 2; i < 70; i++) {
            num3 = num1 + num2;
            num1 = num2;
            num2 = num3;

            System.out.println(num3);
        }
    }
}
